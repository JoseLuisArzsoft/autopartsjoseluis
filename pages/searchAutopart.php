<html>
    <head>
        <title>Serach</title>
        <?php
            session_start();
            include_once('../libraries/pageWeb.php');
            include_once('../libraries/autopart.php');
            $objPage=new PageWeb();
            $objPage->closeSession($_SESSION, '');
            $objAutopart=new Autopart();
            $objPage->linksStyle();
        ?>

    </head>
    <body class="grey darken-4">
        <?php 
            $objPage->menuOptions($_SESSION['user']);
            $objAutopart->consultSearch($_GET, $objPage->searchAutopart());
        ?>
    </body>
</html>