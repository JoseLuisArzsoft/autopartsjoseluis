<html>
    <head>
        <title>Users</title>
        <?php
            session_start();
            include_once('../libraries/pageWeb.php');
            include_once('../libraries/user.php');
            $objPage=new PageWeb();
            $objPage->closeSession($_SESSION, '');
            $objUser=new User();
            $objPage->linksStyle();
        ?>
    </head>
    <body class="grey darken-4">
        <?php
            $objPage->menuOptions($_SESSION['user']);
            $objUser->showUsers($_SESSION['user'], $objPage->searchUser());
        ?>
    </body>
</html>