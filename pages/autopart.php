<html>
    <head>
        <title>Autoparts</title>
        <?php
            session_start();
            include_once('../libraries/pageWeb.php');
            include_once('../libraries/autopart.php');
            $objPage=new PageWeb();
            $objPage->closeSession($_SESSION, '');
            $objAutopart=new Autopart();
            $objPage->linksStyle();
        ?>
        <script>

        document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });
        </script>

    </head>
    <body class="grey darken-4">
        <?php
            $objPage->menuOptions($_SESSION['user']);
            $objAutopart->showAutoparts($objPage->searchAutopart());
        ?>
    </body>
</html>


