<html>
    <head>
        <title>Home Page</title>
        <?php
            session_start();
            include_once('../libraries/pageWeb.php');
            $objPage=new PageWeb();
            $objPage->closeSession($_SESSION, '');
            $objPage->linksStyle();
        ?>
    </head>
    <body class="grey darken-4">
        <?php
            $objPage->menuOptions($_SESSION['user']);
            $objPage->searchAutopart();           
        ?>
    </body>
</html>