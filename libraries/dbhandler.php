<?php
/*
    Project: Auto Parts Webside
    Author: José Luis Márquez Alvarado
    Date: 2021/07/26
    Versión: 1.0
    Description: Website for an auto parts supplier company
    Only registered users should have access. Will exist
    2 types of users, administrators and simple users. The difference
    difference among users is that administrators can
    create / edit and delete users. Within the pot there are also
    They will register / modify and eliminate products of di-
    different categories (Parts, Autoparts and Screws) as well as rea-
    Search by product name or category to consult
    tar your information.
*/
class DBHandler{
    private $strServername = "127.0.0.1";
    private $strUsername = "root";
    private $strPassword = "";
    private $strDBName = "autopartsjoseluis";
    private $objConnection;
    /*
        Description: Method that starts a Database connection
    */
    private function dbConnect(){
        $this->objConnection = new mysqli($this->strServername, $this->strUsername, $this->strPassword, $this->strDBName);

        // Check connection
        if ($this->objConnection->connect_error) {
            echo "Connection failed: " . $this->objConnection->connect_error;
            return false;
        }else{
            return true;
        }
    }
    /*
        Description: Method that executes a query to the Database
    */
    public function queryDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult && $objResult->num_rows > 0) {
                $this->objConnection->close();
                return $objResult;
            }else{
                if($this->objConnection->error){
                    echo "Query error: ".$this->objConnection->error;
                }
                $this->objConnection->close();
                return [];
            }
        }
        return [];
    }
    /*
        Description: Method that executes a insert query to the Database
    */
    public function insertDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult && $this->objConnection->insert_id) {
                $id = $this->objConnection->insert_id;
                $this->objConnection->close();
                return $id;
            }else{
                if($this->objConnection->error){
                    $errorMsg = "Insert error: ".$this->objConnection->error;
                    echo $errorMsg;
                    return $errorMsg;
                }
                $this->objConnection->close();
                return null;
            }
        }
        return null;
    }
    /*
        Description: Method that executes a delete query to the Database
    */
    public function updateDeleteDB($strQuery){
        if ($this->dbConnect()) {
            $objResult = $this->objConnection->query($strQuery);
            
            if ($objResult) {
                $this->objConnection->close();
                return TRUE;
            }else{
                if($this->objConnection->error){
                    $errorMsg = "Update/Delete error: ".$this->objConnection->error;
                    $this->objConnection->close();
                    return $errorMsg;
                }else{
                    $this->objConnection->close();
                    return FALSE;
                }
            }
        }
        return FALSE;
    }
    /*
        Description: Method to print a given input
    */
    public function debug($input){
        echo "<br/><br/>";
        echo "<pre>Result: ".print_r($input, 1)."</pre>";
        echo "<br/><br/>";
    }
}
?>