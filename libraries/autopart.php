<?php

    include_once("dbHandler.php"); 
    class Autopart{
        private $objDBHandler;
        private $strQuery;

        function __construct()
        {
            $this->objDBHandler=new DBHandler();
            $this->strQuery="SELECT * FROM autopart";
        }

        private function tableAutoparts($objResult, $search){
            if($objResult!=[]){
                #divisores
                echo "<div class='row'>";
                echo "<div class='col s6 offset-s3 login-main-contentt'>";
                echo "<div class='card col black white-text'>";
                #divisores
                ////////////////////
                echo "<span class='card-title'>Autoparts</span>";
                echo "$search";
                echo "  <table class='white-text''>";
                echo "      <thead>";
                echo "      <tr>";
                echo "          <th class='collection-item'>Name</th>";
                echo "          <th class='collection-item'>Price</th>";
                echo "          <th class='collection-item'>Quantity</th>";
                echo "          <th class='collection-item'>Category</th>";
                echo "          <th class='collection-item'></th>";
                echo "          <th class='collection-item'></th>";
                echo "          <th></th>";
                echo "      </tr>";
                echo "      </thead>";
                echo "  <tbody>";
                while($autopart = $objResult->fetch_object()){
                    echo "<tr>";
                    echo "  <form action='./autopart/formUpdate.php' method='POST'>";
                    echo "      <input type='hidden' name='Id' value='$autopart->Id'>";
                    echo "      <td class='collection-item'><input type='text' name='name' value='$autopart->name'  class='white-text'></td>";
                    echo "      <td class='collection-item'><input type='text' name='price' value='$autopart->price' class='white-text'></td>";
                    echo "      <td class='collection-item'><input type'text' name='quantity' value='$autopart->quantity' class='white-text'></td>";
                    echo "      <td class='collection-item'>";
                    $this->selectCategory($autopart->category);
                    echo "      </td>";
                    echo "      <td class='collection-item'>";
                    echo "          <button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                    echo "              Update";
                    echo "          </button>";
                    echo "      </td>";
                    echo "  </form>";

                    echo "      <form action='./autopart/formDelete.php' method='POST'>";
                    echo "          <td class='collection-item'>";
                    echo "              <input type='hidden' name='Id' value='$autopart->Id'>";
                    echo "              <button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                    echo "                  Delete";
                    echo "              </button>";
                    echo "          </td>";
                    echo "      </form>";

                    echo "</tr>";
                }
                echo "  </tbody>";
                echo "  </table>";
                ////////////////////
                #divisores
                echo "</div>";
                echo "</div>";
                echo "</div>";
                #divisores
            }
        }

        private function selectCategory($strCategory){
            #echo "<div class='browser-default col s12'>";
            ////////////////////
            echo'       <select id="category" name="category">';
            if($strCategory=='piece'){
                echo "      <option value='piece'>Piece</option>";
                echo "      <option value='autopart'>Autopart</option>";
            }else{
                if($strCategory=='autopart'){
                    echo "  <option value='autopart'>Autopart</option>";
                    echo "  <option value='piece'>Piece</option>";
                }
            }
            echo "          <option value='screw'>Screw</option>";
            if($strCategory=='screw'){
                echo "      <option value='piece'>Piece</option>";
                echo "      <option value='autopart'>Autopart</option>";
            }
            echo "      </select>";
            ////////////////////
            #echo "</div>";
        }

        private function validateLocation($objResult, $conditions){
            if($conditions){
                session_start();
                $_SESSION["Error"] = "";
                $_SESSION["errorMsg"]= "";
                header('Location: ../autopart.php');
                exit;
            }else{
                session_start();
                $_SESSION["Error"] = "Error: The record wasn't updated, please try again. If problem persist, please reach out for support.";
                if(gettype($objResult) == "string"){
                    $_SESSION["errorMsg"] = $objResult;
                }
                header('Location: ./errorAutopart.php');
                exit;
            }
        }

        public function consultSearch($strWord, $searchBotton){
            if($strWord!=''){
                $strQuery="SELECT * FROM autopart WHERE name LIKE '%$strWord[word]%' OR price LIKE '%$strWord[word]%' OR category LIKE '%$strWord[word]%'";
                $objResult = $this->objDBHandler->queryDB($strQuery);
                $this->tableAutoparts($objResult, $searchBotton);
            }
        }

        public function showAutoparts($searchBotton){
            $objResult = $this->objDBHandler->queryDB($this->strQuery);
            $this->tableAutoparts($objResult, $searchBotton);
        }

        public function consultUpdate($arAutopart){
            $strQuery = "UPDATE autopart SET name ='$arAutopart[name]', quantity='$arAutopart[quantity]', price ='$arAutopart[price]', category='$arAutopart[category]' WHERE Id = $arAutopart[Id]";
            $objResult = $this->objDBHandler->updateDeleteDB($strQuery);
            $this->validateLocation($objResult, (gettype($objResult) == "boolean" && $objResult));
        }

        public function consultDelete($Id){
            $strQuery = "DELETE FROM autopart WHERE Id = $Id";
            $objResult = $this->objDBHandler->updateDeleteDB($strQuery);
            $this->validateLocation($objResult, (gettype($objResult) == "boolean" && $objResult));
        }

        public function consultCreate($arAutopart){
            $strQuery="INSERT INTO autopart (name, price, quantity, category) VALUES ('$arAutopart[name]', '$arAutopart[price]', '$arAutopart[quantity]', '$arAutopart[category]')";
            $objResult=$this->objDBHandler->insertDB($strQuery);
            $this->tableAutoparts($objResult, ($objResult != null && is_numeric($objResult)));
        }

        public function debug($input){
            echo "<br/>";
            echo "<pre>Result: ".print_r($input, 1)."</pre>";
            echo "<br/>";
        }

    }
?>