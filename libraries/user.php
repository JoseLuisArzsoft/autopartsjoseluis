<?php
include_once("dbHandler.php");
    class User{
        private $objDBHandler;
        private $strQuery;

        function __construct(){
            $this->objDBHandler=new DBHandler();
            $this->strQuery="SELECT * FROM user";
        }

        private function tableUsers($objResult, $SESSION, $search){
            if($objResult != []){
                #divisores
                echo "<div class='row'>";
                echo "<div class='col s6 offset-s3 login-main-contentt'>";
                echo "<div class='card col black white-text'>";
                #divisores
                ////////////////////
                echo "<span class='card-title'>Users</span>";
                echo "$search";
                echo "<table class='white-text''>";
                echo "<thead>";
                echo "<tr>";
                    echo "<th class='collection-item'>Name</th>";
                    echo "<th class='collection-item'>User Name</th>";
                    echo "<th class='collection-item'>Email</th>";
                    echo "<th class='collection-item'>Password</th>";
                    echo "<th class='collection-item'>Type User</th>";
                    echo "<th class='collection-item'></th>";
                    echo "<th class='collection-item'></th>";
                echo "</tr>";
                echo "</thead>";
                if($SESSION['typeUser']=='ADMINISTRATOR'){
                    while($user = $objResult->fetch_object()){
                        echo "<tr>";
                        echo "<form action='./user/formUpdate.php' method='POST'>";
                        echo "<input type='hidden' name='Id' value='$user->Id'>";
                        echo "<td class='collection-item'><input type='text' name='name' value='$user->name' class='white-text'></td>";
                        echo "<td class='collection-item'><input type='text' name='userName' value='$user->userName' class='white-text'></td>";
                        echo "<td class='collection-item'><input type='email' name='email' value='$user->email' class='white-text'></td>";
                        echo "<td class='collection-item'><input type='password' name='password' value='$user->password' class='white-text'></td>";
                        $this->selectTypeUser($user->typeUser);
                        echo "      <td class='collection-item'>";
                        echo "          <button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                        echo "              Update";
                        echo "          </button>";
                        echo "      </td>";
                        echo "</form>";  
                        
                        echo "<form action='./user/formDelete.php' method='POST'>";
                        echo "<input type='hidden' name='Id' value='$user->Id'>";
                        echo "      <td class='collection-item'>";
                        echo "          <button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                        echo "              Delete";
                        echo "          </button>";
                        echo "      </td>";
                        echo "</form>";
                        echo "</tr>";
                    }
                }else{
                    if($SESSION['typeUser']=='SIMPLE'){
                        while($user = $objResult->fetch_object()){
                            echo "<tr>";
                            echo "<td>$user->name</td>";
                            echo "<td>$user->userName</td>";
                            echo "<td>$user->email</td>";
                            echo "<td>password is private</td>";
                            echo "<td>$user->typeUser</td>";
                            echo "</tr>";
                        }    
                    }
                }
                echo "</table>";
                ////////////////////
                #divisores
                echo "</div>";
                echo "</div>";
                echo "</div>";
                #divisores
            } 
        }

        private function selectTypeUser($typeUser){
            echo "<td>";
            echo "<select id='typeUser' name='typeUser'>"; 
            if($typeUser=='ADMINISTRATOR'){
                echo "<option value='ADMINISTRATOR'>Administrator</option>";
                echo "<option value='SIMPLE'> Simple</option>";
            }else{
                echo "<option value='SIMPLE'> Simple</option>";
                echo "<option value='ADMINISTRATOR'>Administrator</option>";
            }
            echo "</select>";
            echo "</td>";
        }

        private function validateLocation($objResult, $conditions){
            if($conditions){
                session_start();
                $_SESSION["Error"] = "";
                $_SESSION["errorMsg"] = "";
                header('Location: ../user.php');
                exit;
            }else{
                session_start();
                $_SESSION["Error"] = "Error: The record wasn't (updated/deleted/create), please try again. If problem persist, please reach out for support.";
                if(gettype($objResult) == "string"){
                    $_SESSION["errorMsg"] = $objResult;
                }
                header('Location: ./errorUser.php?');
                exit;
            }
        }

        public function showUsers($SESSION, $searchBotton){
            $objResult = $this->objDBHandler->queryDB($this->strQuery);
            $this->tableUsers($objResult, $SESSION, $searchBotton);  
        }

        public function consultUpdate($arUser){
            $strQuery="UPDATE user SET name='$arUser[name]', userName='$arUser[userName]', password='$arUser[password]', email='$arUser[email]', typeUser='$arUser[typeUser]' WHERE Id=$arUser[Id]";
            $objResult = $this->objDBHandler->updateDeleteDB($strQuery);
            $this->validateLocation($objResult, (gettype($objResult) == "boolean" && $objResult));
        }

        public function consultDelete($Id){
            $strQuery = "DELETE FROM user WHERE Id = $Id";
            $objResult = $this->objDBHandler->updateDeleteDB($strQuery);
            $this->validateLocation($objResult, (gettype($objResult) == "boolean" && $objResult));   
        }

        public function consultCreate($arUser){
            $strQuery=("INSERT INTO user (name, userName, password, email, typeUser) VALUES ('$arUser[name]', '$arUser[userName]', '$arUser[password]', '$arUser[email]', '$arUser[typeUser]')");
            $objResult=$this->objDBHandler->insertDB($strQuery);
            $this->validateLocation($objResult, ($objResult != null && is_numeric($objResult)));
        }

        public function consultSearch($strWord, $SESSION, $searchBotton){
            if($strWord!=''){
                $strQuery="SELECT * FROM user WHERE name LIKE '%$strWord[word]%' OR userName LIKE '%$strWord[word]%' OR email LIKE '%$strWord[word]%' OR typeUser LIKE '%$strWord[word]%'";
                $objResult = $this->objDBHandler->queryDB($strQuery);
                $this->tableUsers($objResult, $SESSION, $searchBotton);
            }
        }

        public function debug($input){
            echo "<br/>";
            echo "<pre>Result: ".print_r($input, 1)."</pre>";
            echo "<br/>";
        }
    }
?>