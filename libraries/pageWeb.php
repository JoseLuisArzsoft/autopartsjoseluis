<?php

use function PHPSTORM_META\type;

include_once("dbHandler.php"); 
    
    class PageWeb{
        private $objDBHandler;
        private $arSession;
        
        function __construct(){
            $this->objDBHandler=new DBHandler();
            $this->arSession="";
        }

        public function linksStyle(){
            #echo "<link rel='stylesheet' heref='../style/master.css'>";
            #Compiled and minified CSS
            echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">';
            /*echo '<link rel="stylesheet" href="../style/home.css">';
            #Compiled and minified JavaScript
            echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>';
            echo '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">';
            */
            
            echo "<script>";
            echo "document.addEventListener('DOMContentLoaded', function() {";
            echo "    var elems = document.querySelectorAll('select');";
            echo "    var instances = M.FormSelect.init(elems, options);";
            echo "  });";
            echo "</script>";
        }

        public function validateSession($arUser){
            $strQuery="SELECT * FROM user WHERE name='$arUser[name]' AND password='$arUser[password]'";
            $objResult = $this->objDBHandler->queryDB($strQuery);
            if($objResult){
                session_start();
                while($objUser=$objResult->fetch_object()){
                    $_SESSION['user']=array(
                        'name'=>"$objUser->name",
                        'userName'=>"$objUser->userName",
                        'password'=>"$objUser->password",
                        'email'=>"$objUser->email",
                        'typeUser'=>"$objUser->typeUser"
                    );
                }
                session_start();
                $this->arSession=$_SESSION['user'];
                header("Location: ./pages/homePage.php");
                exit;
            }else{
                header("Location: ./index.html");
            }
        }

        public function menuOptions($SESSION){
                echo "<nav class=' teal'>";
                    echo "<div class='nav-wrapper'>";
                        echo "<a href='homePage.php'></a>";
                        echo "<ul id='right hide-on-med-and-down'>";
                            echo "<li><a href='#' >$SESSION[name]</a></li>";
                            echo "<li><a href='homePage.php'>Home</a></li>";
                            echo "<li><a href='autopart.php'>Autopart</a></li>";
                            echo "<li><a href='newAutopart.php'>New Autopart</a></li>";
                            echo "<li><a href='user.php'>User</a></li>";
            if($SESSION['typeUser']=='ADMINISTRATOR'){
                            echo "<li><a href='newUser.php'>New User</a></li>";
            }
                            echo "<li><a href='closeSession.php'>Close Session</a></li>";
                        echo "</ul>";
                    echo "</div>";
                echo "</nav>";
        }

        public function formNewAutopart(){
                #divisores
                echo "<div class='row'>";
                echo "<div class='col s4 offset-s4 login-main-contentt'>";
                echo "<div class='card col black white-text'>";
                #divisores
                echo "<center><span class='card-title'>New Autopart</span></center>";
                echo '<form action="./autopart/formNew.php" method="POST">';
                    echo '<input type="text" name="name" placeholder="Name" class="white-text">';
                    echo '<input type="text" name="price" placeholder="Price" class="white-text">';
                    echo '<input type="number" name="quantity" placeholder="Quantity" class="white-text">';
                    echo '<select name="category" id="category">';
                        echo '<option value="screw">Screw</option>';
                        echo '<option value="piece">Piece</option>';
                        echo '<option value="autopart">Autopart</option>';
                    echo '</select>';
                    echo "<center><button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                    echo "Create Autopart";
                    echo "</button></center>";
                echo '</form>';
                #divisores
                echo "</div>";
                echo "</div>";
                echo "</div>";
                #divisores
        }

        public function formNewUser($SESSION){
            if($SESSION['typeUser']=='ADMINISTRATOR'){
                #divisores
                echo "<div class='row'>";
                echo "<div class='col s4 offset-s4 login-main-contentt'>";
                echo "<div class='card col black white-text'>";
                #divisores
                echo "<center><span class='card-title'>New User</span></center>";
                echo '<form action="./user/formNew.php" method="POST">';
                    echo '<input type="text" name="name" placeholder="Name" class="white-text">';
                    echo '<input type="text" name="UserName" placeholder="User Name" class="white-text">';
                    echo '<input type="email" name="email" placeholder="Email" class="white-text">';
                    echo '<input type="password" name="password" placeholder="Password" class="white-text">';
                    echo '<select name="typeUser" id="typeUser">';
                        echo '<option value="SIMPLE">Simple</option>';
                        echo '<option value="ADMINISTRATOR">Administrator</option>';
                    echo '</select>';
                    echo "<center><button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                    echo "Create User";
                    echo "</button></center>";
                echo '</form>';
                #divisores
                echo "</div>";
                echo "</div>";
                echo "</div>";
                #divisores
            }
        else{
            if($SESSION['typeUser']=='SIMPLE'){
                header('Location: user.php');
            }else{
                header('Location: ../index.html');
                exit;
            }
            }
        }

        public function searchAutopart(){
            #divisores
            echo "<div class='row'>";
            echo "<div class='col s4 offset-s4 login-main-contentt'>";
            #divisores
            echo "<form action='searchAutopart.php' method='GET'>";
                echo "<input type='text' name='word' placeholder='Search Autopart' class='white-text'>";
                echo "<center><button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                echo "Search Autopart";
                echo "</button></center>";
            echo "</form>";
            #divisores
            echo "</div>";
            echo "</div>";
            #divisores
        }

        public function searchUser(){
            #divisores
            echo "<div class='row'>";
            echo "<div class='col s4 offset-s4 login-main-contentt'>";
            #divisores
            echo "<form action='searchUser.php' method='GET'>";
                echo "<input type='text' name='word' placeholder='Search User' class='white-text'>";
                echo "<center><button class='btn waves-effect waves-light login-btn teal acent-2' type='submit'>";
                echo "Search User";
                echo "</button></center>";
            echo "</form>";
            #divisores
            echo "</div>";
            echo "</div>";
            #divisores
        }

        public function closeSession($SESSION, $request){
            if($SESSION!=[]){
                if($request=='yes'){
                    session_start();
                    $_SESSION=$SESSION;
                    $_SESSION=[];
                    session_destroy();
                    header('Location: ../index.html');
                    exit;
                }else{

                }
            }else{
                if($SESSION==[]){
                    header('Location: ../index.html');
                    exit;
                }
            }
        }

        public function debug($input){
            echo "<br/>";
            echo "<pre>Result: ".print_r($input, 1)."</pre>";
            echo "<br/>";
        }
    }
?>