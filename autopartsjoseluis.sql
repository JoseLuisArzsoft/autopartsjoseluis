-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.19-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para autopartsjoseluis
CREATE DATABASE IF NOT EXISTS `autopartsjoseluis` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `autopartsjoseluis`;

-- Volcando estructura para tabla autopartsjoseluis.autopart
CREATE TABLE IF NOT EXISTS `autopart` (
  `Id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `quantity` int(100) unsigned NOT NULL,
  `price` decimal(20,6) unsigned NOT NULL,
  `category` char(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla autopartsjoseluis.autopart: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `autopart` DISABLE KEYS */;
INSERT INTO `autopart` (`Id`, `name`, `quantity`, `price`, `category`) VALUES
	(1, 'chasis', 4, 100.000000, 'piece'),
	(2, 'frenos', 10, 400.000000, 'autopart'),
	(3, '3/4', 100, 10.000000, 'screw');
/*!40000 ALTER TABLE `autopart` ENABLE KEYS */;

-- Volcando estructura para tabla autopartsjoseluis.user
CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(50) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `userName` char(100) DEFAULT NULL,
  `password` char(100) NOT NULL,
  `email` char(100) NOT NULL,
  `typeUser` char(100) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla autopartsjoseluis.user: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`Id`, `name`, `userName`, `password`, `email`, `typeuser`) VALUES
	(1, 'Arquimidez', 'Arquiz', 'arquiz01', 'arquimidez.mora@arzsystemssoftware.com', 'ADMINISTRATOR'),
	(2, 'Jose Luis', NULL, 'joseluis', 'jose.marquez@arzsystemssoftware.com', 'SIMPLE');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
